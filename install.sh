sudo apt-get install vim vim-addon-manager keepassx git-core terminator ttf-mscorefonts-installer
sudo apt-get install python3-pip makepasswd curl
sudo apt-get install pdf-presenter-console libreoffice
pip3 install httpie --user
mkdir ~/workspace

sudo cp vimrc.local /etc/vim/
sudo cp gitconfig /etc/
